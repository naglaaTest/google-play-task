package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignIn extends PageBase {

	public SignIn(WebDriver driver) {
		super(driver);
		
		// TODO Auto-generated constructor stub
	}
	@FindBy(id="gb_70")
	//@FindBy(xpath="//*[@id=\"wrapper\"]/div[4]/div/div[2]/div/div[2]/div[5]/a/div/span")
	WebElement signInBtn ;
	
	//@FindBy (id="identifierId")
	@FindBy (xpath="//*[@id=\"identifierId\"]")
	WebElement name;
	
	//@FindBy (css="content.CwaK9")
	@FindBy(xpath="//*[@id=\"identifierNext\"]/content/span")
	WebElement NextBbtn ;
		
	@FindBy (name ="password")
	WebElement Password ;
	
	@FindBy (css="content.CwaK9")
	WebElement NextBbtn2 ;
	
	
	// in case you want to sign in with new account you can use this method and pass Email and password as parameter 
	public void SignInwith (String Email , String PasswordWord) throws InterruptedException
	{
		signInBtn.click();
		Thread.sleep(2000);
		name.sendKeys(Email);
		NextBbtn.click();
		Thread.sleep(3000);
		Password.sendKeys(PasswordWord);
		NextBbtn2.click();
	
	}
	
}
