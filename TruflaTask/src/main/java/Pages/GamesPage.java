package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Tests.TestBase;

public class GamesPage extends TestBase{



 // select first game
	public static void GetFirstGame() throws InterruptedException
	{   
		driver.navigate().refresh();
		List<WebElement> Games = driver.findElements(By.cssSelector("a.title"));
		System.out.println(Games.size());
		for (WebElement webElement : Games) 
		{
			String x = webElement.getAttribute("title");
			System.out.println(x);

		}

		WebElement Game = Games.get(0);
		Game.click();
		Thread.sleep(4000);  		
		WebElement Add = driver.findElement(By.cssSelector("button.V2rsl"));
		Add.click();

	}
	
// select last game
	public static void AddGamestoWishlist() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"body-content\"]/div/div/div[1]/div[2]/div[1]/div/div[1]/h2/span/a")));
		WebElement MoreBtn = driver.findElement(By.xpath("//*[@id=\"body-content\"]/div[1]/div/div[1]/div[2]/div[1]/div/div[1]/h2/span/a"));
		MoreBtn.click();
		Thread.sleep(3000);	
		List<WebElement> Games = driver.findElements(By.cssSelector("a.title"));
		System.out.println(Games.size());
		int length = Games.size()-1;
		WebElement lastGame = Games.get(length);
		lastGame.click();
		Thread.sleep(4000); 
		WebElement Add = driver.findElement(By.cssSelector("button.V2rsl"));
		Add.click();
		Thread.sleep(4000);
		/*/ future work we need to check compatibility of game with device
		String GameMsg = driver.findElement(By.xpath("//*[@id=\"ow27\"]/content/span")).getText();
		if (GameMsg.equals("This app is incompatible with your device."))
		{
			driver.navigate().back();
		}
		/*/


	}
}
