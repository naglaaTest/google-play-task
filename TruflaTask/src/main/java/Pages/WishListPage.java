package Pages;

import java.awt.Window;
import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import Tests.TestBase;

public class WishListPage extends TestBase {


	public void SelectGameGromWishList() throws InterruptedException
	{
		driver.navigate().refresh();
		List<WebElement> Games = driver.findElements(By.cssSelector("a.title"));
		WebElement Game = Games.get(0);
		Game.click();
		Thread.sleep(4000);  	
	}
	public String ChooseToInstall()
	{
		String GameName = driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[1]/c-wiz[1]/h1/span")).getText();
		System.out.println(GameName);
		WebElement InstallBbtn = driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div[2]/c-wiz/div/span/button"));
		InstallBbtn.click();
		return GameName;
	}


	public void InstallGame() throws InterruptedException 
	{

		WebElement active =	driver.switchTo().activeElement();
		active.click();
		//WebElement window = driver.findElement(By.xpath("/html/body/div[9]/div[1]"));
		//WebElement clickOk = driver.findElement(By.id("purchase-ok-button"));
		//clickOk.click();

		active.sendKeys(Keys.TAB);
		active.sendKeys(Keys.TAB);
		active.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		WebElement OK =	 driver.switchTo().activeElement();
		OK.click();
		OK.sendKeys(Keys.TAB);
		OK.sendKeys(Keys.ENTER);



		//WebElement menu = driver.findElement(By.xpath("/html/body/div[9]/div[1]"));
		//WebElement InstallGame = driver.findElement(By.xpath("//*[@id=\"purchase-ok-button\"]"));
		//("purchase-ok-button"));

		//InstallGame.click();

	}
	public void WriteReview(String SelectdGame) throws InterruptedException 

	{
		// As button of review doesn't display in page atomatic after game installed , i have to refresh page so , 
		//instead i go to my app page and select game then click on add review
		Thread.sleep(36000);
		WebElement MyApp = driver.findElement(By.linkText("My apps"));
		MyApp.click();
		Thread.sleep(3000);
		List<WebElement> InstalledGames = driver.findElements(By.cssSelector("a.title"));
		System.out.println(InstalledGames.size());
		for (WebElement App : InstalledGames) 
		{
			String x = App.getAttribute("title");
			System.out.println(x);
			if (x.equals(SelectdGame))
			{
				App.click();
				break;
			}

		}
		Wait<WebDriver> fwait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofMinutes(10))
				.pollingEvery(Duration.ofSeconds(60))
				.ignoring(NoSuchElementException.class);
		WebElement WriteReview = fwait.until(new Function<WebDriver,WebElement>()
		{ public WebElement apply (WebDriver d)
		{

			return d.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div[1]/div/div/div[1]/div[1]/span/button"));


		}


		});


		WriteReview.click();


	}
	public void VerifyPassword(String SelectdGame, String PasswordVerify ) throws InterruptedException
	{
		
		String title = driver.getTitle();
		System.out.println(title);
		if (driver.getTitle().contains(SelectdGame))
		{
			InstallGame();


		}
		else if (driver.getTitle().contains("Google Play"))

		{
			WebElement VerifyPasswordtxt = driver.findElement(By.name("password"));
			VerifyPasswordtxt.sendKeys(PasswordVerify);
			WebElement nextBtn = driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content/span"));
			nextBtn.click();
			Thread.sleep(3000);
			InstallGame();

		}

	}

	public void ReviewGame(String Review) throws InterruptedException 
	{
		Thread.sleep(5000);
		

		// driver.switchTo().defaultContent();
		 WebElement activelement = driver.switchTo().activeElement();
		 activelement.click();
		// activelement.sendKeys(Review);
		// WebElement frame = driver.findElement(By.xpath("//*[@id=\"I0_1539362986770\"]"));
		// driver.switchTo().frame(frame);
		WebElement Window =  driver.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div/div[1]/div/div/div[4]/div/div[2]/div[2]/div[4]/div/div/div[1]/div[3]/button[5]"));
		Window.click();
		WebElement RateStar =driver.findElement(By.cssSelector("button.fifth-star"));
		RateStar.click();

		WebElement TextReview= driver.findElement(By.cssSelector("textarea.review-input-text-box.write-review-comment"));
		TextReview.sendKeys(Review);
		WebElement SubmitBtn = driver.findElement(By.cssSelector("button.id-submit-review.play-button.apps"));
		SubmitBtn.click();


	}
}




