package Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AppsPage extends PageBase {

	public AppsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(linkText="Apps")
	WebElement AppsLink;
	
	@FindBy(linkText="Games")
	WebElement GamesLink;
	
	@FindBy(linkText="My wishlist")
	WebElement Wishlist;
	
	
	public void SelectApps()
	{
		AppsLink.click();
		
	}
	
	public void ChooseGames() throws InterruptedException
	{

		GamesLink.click();
		
	}
	
	public void SelectMyWishList()
	{
		Wishlist.click();
		
	}


}
