package Tests;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class TestBase
{
	static String ChromePath = (System.getProperty("user.dir"));
	String AppdataFolder = System.getenv("LOCALAPPDATA");
 String	AppDataFolderLoc = AppdataFolder+"\\Google\\Chrome\\User Data\\Default";
	
	public static WebDriver driver;


@BeforeTest
public static void launchBrowserAndChooseTologin() throws InterruptedException
{ // here i select defualt profile of chrome and launch it note , if you launch it and didn;t display logged user , then you have to 
	// launch it then sign it then clode it and run test again ir will launch sucessfully
	System.out.println(ChromePath);
	System.out.println("launching chrome browser"); 
	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
	ChromeOptions options = new ChromeOptions();
    options.addArguments("user-data-dir=AppDataFolderLoc");
    options.addArguments("disable-infobars");
    options.addArguments("--start-maximized");
     driver = new ChromeDriver(options);
	driver.navigate().to("https://play.google.com/store");
	

}
public static void UrlRefresh()
{
	driver.navigate().refresh();
}

@AfterTest
public static void Close_Drive()
{
      driver.quit();

}
}

